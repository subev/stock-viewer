import axios from "axios";
import classNames from "classnames";
import { useCallback, useEffect, useState } from "react";
import "./App.css";

type Ticker = string;
type Deviation = number | undefined;

type Price = {
  ask: number;
  bid: number;
  lastVol: number;
  open: number;
  symbol: Ticker;
};

type GridColumn = {
  title: string;
  field: keyof Price;
};

type Notification = {
  time: Date;
  ticker: Ticker;
  deviation: NonNullable<Deviation>;
};

const BASE_URL = "http://localhost:3000";
const DEFAULT_DEVIATION = 0.03;
const POLLING_INTERVAL_MS = 1000;

const App = () => {
  const [isLoading, setIsLoading] = useState(true);
  const [selectedTab, setSelectedTab] = useState(0);
  const [availableTickers, setAvailableTickers] = useState<Ticker[]>([]);
  const [watchlist, setWatchlist] = useState<Ticker[]>(["MSFT.O", "VRSN.O"]);
  const [prices, setPrices] = useState<Record<Ticker, Price>>({});
  const [alerts, setAlerts] = useState<Record<Ticker, Deviation>>({});
  const [tickerInEdit, setTickerInEdit] = useState<Ticker | undefined>();
  const [notifications, setNotifications] = useState<Array<Notification>>([]);

  const handleAddToWatchlist = useCallback((ticker: Ticker) => {
    setWatchlist((s) => [...s, ticker]);
    axios
      .get<Price>(`${BASE_URL}/price/${ticker}`)
      .then((x) => x.data)
      .then((x) => setPrices((s) => ({ ...s, [ticker]: x })));
  }, []);

  const handleRemove = useCallback((idx: number) => {
    setWatchlist((s) => {
      const result = s.slice();
      result.splice(idx, 1);
      return result;
    });
  }, []);

  const handleAlertUpdate = useCallback(
    (ticker: Ticker, deviation: Deviation) => {
      setAlerts((s) => ({
        ...s,
        [ticker]: deviation,
      }));
    },
    []
  );

  // initial fetch for the watchlist
  useEffect(() => {
    axios
      .get<Ticker[]>(`${BASE_URL}/static/tickers`)
      .then((x) => x.data)
      .then((x) => setAvailableTickers(x));
  }, []);

  // fetch items for the add symbol dropdown
  useEffect(() => {
    setIsLoading(true);
    axios
      .get<Array<Price>>(`${BASE_URL}/prices/${watchlist.join(",")}`)
      .then((x) => x.data)
      .then((response) =>
        setPrices(Object.fromEntries(response.map((x) => [x.symbol, x])))
      )
      .finally(() => setIsLoading(false));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  // long poll the alerts if any
  useEffect(() => {
    const intervalId = setInterval(() => {
      const alertTickersToWatch = Object.entries(alerts).filter(([_t, d]) => d);
      if (alertTickersToWatch.length) {
        console.log("long polling");
        axios
          .get<Array<Price>>(
            `${BASE_URL}/prices/${alertTickersToWatch
              .map(([t]) => t)
              .join(",")}`
          )
          .then((x) => x.data)
          .then((response) => {
            // merge new prices into existing
            setPrices((s) => ({
              ...s,
              ...Object.fromEntries(response.map((x) => [x.symbol, x])),
            }));
            // add notifications if any new
            const existingNotifications = new Set(
              notifications.map((x) => x.ticker)
            );
            const newNotifications = response.reduce<Notification[]>(
              (acc, { symbol, open, bid }) => {
                const deviation = open / bid - 1;
                if (
                  Math.abs(deviation) > (alerts[symbol] ?? Infinity) &&
                  !existingNotifications.has(symbol)
                ) {
                  acc.push({
                    ticker: symbol,
                    deviation,
                    time: new Date(),
                  });
                }
                return acc;
              },
              []
            );
            setNotifications((s) => [...s, ...newNotifications]);
          });
      }
    }, POLLING_INTERVAL_MS);
    return () => clearInterval(intervalId);
  }, [alerts, notifications]);

  return (
    <div className="App">
      <h1>Stock Viewer</h1>
      <Switcher selected={selectedTab} onSelect={setSelectedTab} />
      {selectedTab === 0 && !isLoading && (
        <div>
          <Table
            watchlist={watchlist}
            prices={prices}
            alerts={alerts}
            onRemove={handleRemove}
            onAlertUpdate={handleAlertUpdate}
            onAlertEdit={setTickerInEdit}
          />
          {availableTickers.length && (
            <SymbolPicker
              setWatchlist={setWatchlist}
              options={availableTickers}
              onAdd={handleAddToWatchlist}
            />
          )}
        </div>
      )}
      {selectedTab === 1 && (
        <Alerts
          notifications={notifications}
          onClear={(t) =>
            setNotifications((s) => s.filter((n) => n.ticker !== t))
          }
        />
      )}
      {tickerInEdit && (
        <Modal
          deviation={alerts[tickerInEdit]}
          ticker={tickerInEdit}
          onAlertUpdate={handleAlertUpdate}
          onClose={() => setTickerInEdit(undefined)}
        />
      )}
    </div>
  );
};

const Switcher = ({
  selected,
  onSelect,
}: {
  selected: number;
  onSelect: (n: number) => void;
}) => {
  const items = ["Stocks", "Alerts"];

  return (
    <div className="TabStrip">
      {items.map((i, idx) => (
        <div
          key={idx}
          className={classNames("TabStripItem", { Selected: idx === selected })}
          onClick={() => onSelect(idx)}
        >
          {i}
        </div>
      ))}
    </div>
  );
};

const Table = ({
  watchlist,
  prices,
  alerts,
  onRemove,
  onAlertUpdate,
  onAlertEdit,
}: {
  watchlist: Ticker[];
  prices: Record<Ticker, Price>;
  alerts: Record<Ticker, Deviation>;
  onRemove: (t: number) => void;
  onAlertUpdate: (t: Ticker, d: Deviation) => void;
  onAlertEdit: (t: Ticker) => void;
}) => {
  const columns: Array<GridColumn> = [
    { title: "Stock", field: "symbol" },
    { title: "Bid", field: "bid" },
    { title: "Ask", field: "ask" },
    { title: "Vol", field: "lastVol" },
  ];

  return (
    <div className="Grid">
      <table>
        <thead>
          <tr>
            {columns.map((c, idx) => (
              <th key={idx}>{c.title}</th>
            ))}
            <th>Alerts</th>
            <th>Options</th>
          </tr>
        </thead>
        <tbody>
          {watchlist.map((ticker, idx) => (
            <TickerRow
              ticker={ticker}
              index={idx}
              key={idx}
              columns={columns}
              price={prices[ticker]}
              alert={alerts[ticker]}
              onRemove={onRemove}
              onAlertUpdate={onAlertUpdate}
              onAlertEdit={onAlertEdit}
            />
          ))}
        </tbody>
      </table>
    </div>
  );
};

const TickerRow = ({
  ticker,
  columns,
  price,
  index,
  alert,
  onRemove,
  onAlertUpdate,
  onAlertEdit,
}: {
  ticker: Ticker;
  columns: Array<GridColumn>;
  price: Price;
  index: number;
  alert: Deviation;
  onRemove: (t: number) => void;
  onAlertUpdate: (t: Ticker, d: Deviation) => void;
  onAlertEdit: (t: Ticker) => void;
}) => {
  const hasPrice = Boolean(price);

  return (
    <tr>
      {columns.map((c, idx) => (
        <td key={idx}>{hasPrice ? format(price[c.field], c.field) : "..."}</td>
      ))}
      <td>
        {
          // eslint-disable-next-line jsx-a11y/anchor-is-valid
          <a href="#">
            <input
              type="checkbox"
              checked={alert !== undefined}
              onChange={(e) =>
                onAlertUpdate(
                  ticker,
                  e.target.checked ? DEFAULT_DEVIATION : undefined
                )
              }
            />
          </a>
        }
      </td>
      <td>
        {
          // eslint-disable-next-line jsx-a11y/anchor-is-valid
          <a href="#" onClick={() => onAlertEdit(ticker)}>
            Alerts
          </a>
        }{" "}
        {
          // eslint-disable-next-line jsx-a11y/anchor-is-valid
          <a href="#" onClick={() => onRemove(index)}>
            X
          </a>
        }
      </td>
    </tr>
  );
};

const SymbolPicker = ({
  options,
  onAdd,
}: {
  options: Ticker[];
  setWatchlist: React.Dispatch<React.SetStateAction<Ticker[]>>;
  onAdd: (i: Ticker) => void;
}) => {
  const [selected, setSelected] = useState<Ticker>(options[0]);

  return (
    <div className="SymbolPicker">
      Add Symbol:
      <select
        className="SymbolPickerInput"
        onChange={(x) => setSelected(x.target.value)}
      >
        {options.map((o, idx) => (
          <option key={idx} value={o}>
            {o}
          </option>
        ))}
      </select>
      <button className="SymbolPickerAdd" onClick={() => onAdd(selected)}>
        Add
      </button>
    </div>
  );
};

const Modal = ({
  ticker,
  onClose,
  onAlertUpdate,
  deviation,
}: {
  ticker: Ticker;
  onClose: () => void;
  onAlertUpdate: (t: Ticker, d: Deviation) => void;
  deviation: Deviation;
}) => {
  const [percentage, setPercentage] = useState(deviation || DEFAULT_DEVIATION);
  const [isChecked, setIsChecked] = useState(Boolean(deviation));

  return (
    <div className="Modal">
      <div className="Content">
        <h2>Edit Alert</h2>
        <label>
          <input
            type="checkbox"
            checked={isChecked}
            onChange={(e) => setIsChecked(e.target.checked)}
          />{" "}
          Alert me when the stock price deviates from the opening price by:
        </label>
        <div className="ModalPercentageInput">
          <input
            type="number"
            value={(percentage * 100).toFixed(2)}
            onChange={(e) => setPercentage(Number(e.target.value) / 100)}
          />
          %
        </div>
        <div className="ModalCloseButton">
          <button
            onClick={() => {
              onAlertUpdate(ticker, isChecked ? percentage : undefined);
              onClose();
            }}
          >
            OK
          </button>
        </div>
      </div>
    </div>
  );
};

const Alerts = ({
  notifications,
  onClear,
}: {
  notifications: Array<Notification>;
  onClear: (t: Ticker) => void;
}) => {
  return (
    <div className="Alerts">
      {notifications.map(({ ticker, deviation, time }, idx) => (
        <div key={idx} className="Notification">
          <div className="Title">
            <div className="Text">
              <strong>{ticker}</strong> @ {time.toLocaleTimeString()}
            </div>
            <div className="CloseIcon" onClick={() => onClear(ticker)}>
              X
            </div>
          </div>
          <strong>{ticker}</strong> BID price moved{" "}
          <strong>{(deviation * 100).toFixed(2)}%</strong> from the opening
          price at {time.toLocaleTimeString()} today
        </div>
      ))}
    </div>
  );
};

const format = (x: string | number, field: keyof Price) =>
  typeof x === "number" && (field === "ask" || field === "bid")
    ? x.toFixed(2)
    : x;

export default App;
